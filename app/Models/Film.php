<?php
namespace App\Models;
use CodeIgniter\Model;

class Film extends Model {
    protected $table = 'film';
    protected $returnType = 'object'; // default array
    protected $protectFields = false;
    // by default pk = id
    protected $primaryKey = 'film_id';
    //protected $allowedFields = ['title', 'description'];
}