<?php
namespace App\Models;
use CodeIgniter\Model;

class AuditTrail extends Model {
    protected $table = 'audit_trail';
    protected $returnType = 'object';
    protected $protectFields = false;
}