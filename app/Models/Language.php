<?php
namespace App\Models;
use CodeIgniter\Model;

class Language extends Model {
    protected $table = 'language';
    protected $returnType = 'object';

    // return array ['EN' => 1, 'BM' => 2]
    function getData() {
        $arr = $this->whereIn('language_id', [1,2,3])->findAll();
        $rows = [];
        foreach ($arr as $data) {
            $rows[$data->name] = $data->language_id;
        }
        return $rows;
    }
}