<?php
namespace App\Libraries;

class Form {
    // return dropdown box
    public static function select($name, $data, $value, $options = array()) {
        $str  = "<select name='$name' class='form-control'>";
        $str .= "<option value=''>-- Please Choose --</option>";

        foreach ($data as $key => $val) {
            $selected = $value == $val ? 'selected' : '';
            $str .= "<option value='$val' $selected>$key</option>";
        }
        
        $str .= "</select>";
        return $str;
    }
}