<?= $this->extend('layout') ?>
<?= $this->section('title') ?> New Film <?= $this->endSection() ?>
<?= $this->section('content') ?>

<form action="<?= url_to('film/save') ?>" method="post">
    <input type="hidden" name="film_id" value="<?= $film['film_id'] ?? '' ?>">
    
    <!-- display error message here -->
    <?php if (isset($validator)) : ?>
        <div class="alert alert-danger">
            <?= $validator->listErrors() ?>
        </div>
    <?php endif; ?>

    <div class="row mb-1">
        <div class="col-md-2">Title</div>
        <div class="col-md-10">
            <input value="<?= $film['title'] ?? '' ?>" name="title" type="text" class="form-control">
        </div>
    </div>
    <div class="row mb-1">
        <div class="col-md-2">Description</div>
        <div class="col-md-10">
            <textarea name="description" class="form-control"><?= $film['description'] ?? '' ?></textarea>
        </div>
    </div>
    <div class="row mb-1">
        <div class="col-md-2">Language</div>
        <div class="col-md-10">
            <?= \App\Libraries\Form::select('language_id', $languages, $film['language_id'] ?? '') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-10">
            <input type="submit" class="btn btn-primary">
        </div>
    </div>
</form>

<?= $this->endsection() ?>