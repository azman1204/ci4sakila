<?= $this->extend('layout') ?>

<?= $this->section('title') ?>
Film List
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<form method="post" action="<?= url_to('film') ?>" class="mb-2">
    <div class="row">
        <div class="col-md-1">Title</div>
        <div class="col-md-4">
            <input type="text" class="form-control" name="title" value="<?= $_POST['title'] ?? '' ?>">
        </div>
        <div class="col-md-1"><input type="submit" class="btn btn-primary" value="Search"></div>
    </div>
</form>

<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>No</th>
            <th>Title</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = isset($_GET['page']) ? ($_GET['page'] - 1) * 20 + 1 : 1;
        foreach($films as $film) : ?>
        <tr>
            <td> <?= $no++ ?></td>
            <td> <?= $film->title ?></td>
            <td> <?= $film->description ?></td>
            <td>
                <a class="btn btn-primary" href="<?= base_url("film/edit/".$film->film_id) ?>">Edit</a>
                <a onclick="return confirm('Are you sure ?')" class="btn btn-danger" href="<?= base_url("film/delete/".$film->film_id) ?>">Delete</a>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?= $pager->links() ?>
<?= $this->endSection() ?>