<?= $this->extend('layout') ?>
<?= $this->section('content') ?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<table id="example">
    <thead>
        <th>No</th>
        <th>Title</th>
        <th>Actor</th>
    </thead>
    <tbody>
        <?php 
        $no = 1;
        foreach ($films as $film) : ?>
        <tr>
            <td><?= $no++ ?></td>
            <td><?= $film->title2 ?></td>
            <td><?= $film->actor2 ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<script src='https://code.jquery.com/jquery-3.5.1.js'></script>
<script src='https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js'></script>
<script>
    $(document).ready(function () {
        $('#example').DataTable();
    });
</script>
<?= $this->endSection() ?>