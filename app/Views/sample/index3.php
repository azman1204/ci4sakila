<?= $this->extend('layout') ?>
<?= $this->section('content') ?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">

<table id="example">
    <thead>
        <th>No</th>
        <th>Title</th>
        <th>Description</th>
    </thead>
</table>

<script src='https://code.jquery.com/jquery-3.5.1.js'></script>
<script src='https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js'></script>
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            processing: true,
            serverSide: true,
            ajax: '<?= url_to('data-demo3') ?>',
        });
    });
</script>
<?= $this->endSection() ?>