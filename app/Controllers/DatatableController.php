<?php
namespace App\Controllers;
use App\Controllers\BaseController;
use stdClass;

class DatatableController extends BaseController {
    // example datatable - no config - return all data
    public function index() {
        // teknik QueryBuilder
        $db = \Config\Database::connect();
        $rs = $db->table('film')
                ->join('film_actor', 'film_actor.film_id = film.film_id')
                ->join('actor', 'actor.actor_id = film_actor.actor_id')
                ->orderBy('title', 'asc')
                ->get();
        //dd($rs->getResult());
        $films = $rs->getResult();
        return view('sample/index', compact('films'));
    }

    public function index2() {
        // teknik QueryBuilder
        $db = \Config\Database::connect();
        $rs = $db->table('film')
                ->orderBy('title', 'asc')
                ->get();
        $films = $rs->getResult();

        $arr = [];
        foreach ($films as $film) {
            $actors = $db->table('actor')
                      ->join('film_actor', 'film_actor.actor_id = actor.actor_id')
                      ->where('film_id', $film->film_id)
                      ->get()
                      ->getResult();
            $str_actor = '';
            foreach ($actors as $actor) {
                $str_actor .= "<li>$actor->first_name  $actor->last_name</li>";
            }
            $obj = new stdClass();
            $obj->title2 = $film->title;
            $obj->actor2 = $str_actor;
            $arr[] = $obj;
        }

        return view('sample/index2', ['films' => $arr]);
    }

    // display page datatable
    public function serverSide() {
        return view('sample/index3');
    }

    // feed data to page datatable. using AJAX
    public function getData() {
        // to return draw, recordsTotal, recordsFilteres, data
        $db = \Config\Database::connect();
        $title = $_GET['search']['value'];

        // select * from film where title like '%aca%' limit 10, 0
        $builder = $db->table('film')->like('title', $title);
        $films = $builder->get($_GET['length'], $_GET['start'])->getResult();
        $data = [];
        $no = 1;
        foreach ($films as $film) {
            $arr = [$no++, $film->title, $film->description];
            $data[] = $arr;
        }

        // select count(film_id) from film
        $builder2 = $db->table('film')->selectCount('film_id');
        $rows = $builder2->get()->getResult();
        //dd($rows);

        // return semua data yg di filtered
        // select * from film where title like '%aca%'
        $builder3 = $db->table('film')->like('title', $title);
        $rows2 = $builder3->get()->getResult();

        $obj = new \stdClass();
        $obj->draw = $_GET['draw'];
        $obj->recordsFiltered = count($rows2);
        $obj->recordsTotal = $rows[0]->film_id;
        $obj->data = $data;
        //echo "<pre>" . json_encode($obj) . '</pre>';
        echo json_encode($obj);
    }
}