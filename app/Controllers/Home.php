<?php
namespace App\Controllers;
use App\Models\Staff;
class Home extends BaseController {
    public function index() {
        return view('login');
    }

    public function login() {
        $model = new Staff();
        // first return an object
        $staff = $model->where('email', $_POST['email'])->first();
        if ($staff) {
            // found a staff, check password
            if (password_verify($_POST['password'], $staff->password)) {
                // password correct
                session()->set('logged-in', true);
                return redirect('film');
            } else {
                // wrong password
                return view('login', ['err' => 'Wrong credentials']);
            }
        } else {
            // not found
            return view('login', ['err' => 'Wrong credentials']);
        }

        //echo password_hash('1234', PASSWORD_DEFAULT);
    }

    function logout() {
        // clear all session
        session()->destroy();
        return redirect('/');
    }
}
