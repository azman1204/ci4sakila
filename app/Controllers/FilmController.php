<?php
namespace App\Controllers;
use App\Controllers\BaseController;
use App\Models\Film;
use App\Models\Language;

class FilmController extends BaseController {
    private $film;

    // constructor - always run before run other funtion
    public function __construct() {
        $this->film = new Film();
    }

    public function index() {
        if ($this->request->getMethod() == 'post') {
            // on click search button
            $films = $this->film->like("title", $_POST['title'])->paginate(20);
        } else {
            // on click from menu
            $films = $this->film->paginate(20);
        }

        $pager = $this->film->pager;
        return view('film/index', compact('films', 'pager'));
    }

    // on click create new film
    public function create() {
        $lang = new Language();
        $languages = $lang->getData();
        return view('film/form', compact('languages'));
    }

    public function save() {
        $film_id = $_POST['film_id'];

        // validation
        $ok = $this->validate([
            'title'       => 'required',
            'description' => 'required',
            'language_id' => 'required'
        ]);

        if ($ok) {
            if (empty($film_id)) {
                // insert
                $this->film->insert($_POST);
            } else {
                // update
                $this->film->update($film_id, $_POST);
            }
            return redirect('film');
        } else {
            // not ok -- ada error
            $lang = new Language();
            $languages = $lang->getData();
            $data = [
                'validator' => $this->validator,
                'film' => $_POST,
                'languages' => $languages
            ];
            return view('film/form', $data);
        }
    }

    public function edit($film_id) {
        $f = (array) $this->film->find($film_id); // find() - cari by primary key,return an obj
        $lang = new Language();
        $languages = $lang->getData();
        return view('film/form', ['film' => $f, 'languages' => $languages]);
    }

    public function delete($film_id) {
        $this->film->delete($film_id);
        return redirect('film');
    }
}