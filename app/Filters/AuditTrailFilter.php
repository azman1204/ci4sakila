<?php

namespace App\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use App\Models\AuditTrail;

class AuditTrailFilter implements FilterInterface {
    public function before(RequestInterface $request, $arguments = null) {
        $trail = new AuditTrail();
        $data = [];

        if (session()->has('logged-in')) {
            $data['user_name'] = '?';
        } else {
            $data['user_name'] = 'anonymous';
        }

        $data['created_at'] = date('Y-m-d H:i:s');
        $uri = $request->getUri();
        $data['url'] = $uri->getPath();
        $data['data'] = json_encode($_POST);
        $data['ip'] = $request->getIpAddress();
        $trail->insert($data);
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        //
    }
}
