<?php
namespace App\Database\Migrations;
use CodeIgniter\Database\Migration;
use PHPUnit\Framework\Constraint\Constraint;

class CreateAuditTrailTable extends Migration {
    // run migrate
    public function up()
    {
        $this->forge->addField('id');
        $this->forge->addField([
            'created_at'=> ['type' => 'datetime'],
            'user_name' => ['type' => 'varchar', 'constraint' => 50],
            'url'       => ['type' => 'varchar', 'constraint' => 100],
            'data'      => ['type' => 'text'],
            'ip'        => ['type' => 'varchar', 'constraint' => 15],
            'prev_page' => ['type' => 'varchar', 'constraint' => 100],
            'agent'     => ['type' => 'varchar', 'constraint' => 100],
            'method'    => ['type' => 'varchar', 'constraint' => 10],
        ]);
        $this->forge->createTable('audit_trail', true);
    }

    // run rollback
    public function down()
    {
        $this->forge->dropTable('audit_trail');
    }
}
