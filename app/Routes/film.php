<?php
$routes->group('/film', ['filter' => 'login'], function($routes) {
    $routes->add('', 'FilmController::index');
    $routes->get('create', 'FilmController::create');
    $routes->post('save', 'FilmController::save');
    $routes->get('edit/(:num)', 'FilmController::edit/$1');
    $routes->get('delete/(:num)', 'FilmController::delete/$1');
});