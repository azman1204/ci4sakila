<?php
$routes->get('/', 'Home::index');
$routes->post('login', 'Home::login');
$routes->get('logout', 'Home::logout');

// example data tables
$routes->get('datatable-demo', 'DatatableController::index');
$routes->get('datatable-demo2', 'DatatableController::index2');

$routes->get('datatable-demo3', 'DatatableController::serverSide');
$routes->get('data-demo3', 'DatatableController::getData');